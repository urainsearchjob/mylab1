using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character : MonoBehaviour
{
	[SerializeField] private float speed = 3.0f; // speed of move 
	[SerializeField] private int lives = 5; // lives
	[SerializeField] private float jumpForce = 15.0f; // fight of jump
    private bool isGrounded = false; // jump limit

	private Rigidbody2D rb;
    private Animator animator;
	private SpriteRenderer sprite;
    
    private void Awake()  // get component
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();

    }
   private void FixedUpdate() //для вызова методов через фикс. промеж. времени
    {
    	CheckGround();
    }

private void Update()  // для контроля нажатий
{
	if (Input.GetButton("Horizontal"))
Run();
    if (isGrounded && Input.GetButtonDown("Jump"))  // Input.GetButtonDown("Jump")  
     /// rb.AddForce (Vector2.up * 1);
    Jump();
}
    // Update is called once per frame
    private void Run()
    {
        Vector3 dir = transform.right * Input.GetAxis("Horizontal");
                                                       // местоположение, местоположение при нажатии, скорость
        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, speed * Time.deltaTime);
    sprite.flipX = dir.x < 0.0f; //поворот персонажа, куда идет
    }
    private void Jump()
    {rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);} 
   private void CheckGround()
    {
    	Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f);
    	isGrounded = colliders.Length > 1;
    } 

}

